import { createRouter, createWebHistory } from "vue-router";

import HomeIndex from "../views/HomeIndex";

const routes = [
    {
        path: '/',
        name: 'index',
        component: HomeIndex
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})
