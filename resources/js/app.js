require('./bootstrap');

import { createApp } from "vue";
import router from "./router";
import App from './App.vue'
import AppLayout from "./layouts/AppLayout";

createApp(App)
    .component('AppLayout', AppLayout)
    .use(router)
    .mount('#app')
