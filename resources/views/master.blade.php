<!doctype html>
<html lang="hu">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <title></title>
</head>
<body>
    <noscript>
        <strong>Sajnáljuk, de az oldal JavaScript nélkül nem megjeleníthető.</strong>
    </noscript>

    <div id="app"></div>

    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
